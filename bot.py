import discord
from confighandler import config

selInfo = {}
client = discord.Client()


class MessageAlreadyExists(Exception):

    def __init__(self, err):
        print(err)
        pass


async def RoleSelectMenu(guild):
    em_info = config.emget(guild.id)
    embed = discord.Embed(title=em_info("title"),
                          colour=discord.Colour(0x1ba4),
                          url=str(em_info("url")),
                          description=em_info("description")
                          )

    roledesc = em_info("roledesc").split("|")
    roleemoji = em_info("roleemoji").split("|")

    for i in range(len(roledesc)):
        embed.add_field(name=roleemoji[i], value=roledesc[i], inline=True)

    return embed


async def SetupSelectionMsg(GuildnChannel: tuple):
    c, s = GuildnChannel
    await c.send(embed=await RoleSelectMenu(s))


@client.event
async def on_ready():
    global selInfo
    print("I'm logged into: " + config.get("ServerID"))
    print(client.user)
    servers = config.get("ServerID").split("|")
    channels = config.get("SelectionChannelID").split("|")
    for i in range(len(servers)):
        selInfo[client.get_guild(int(servers[i]))] = client.get_channel(int(channels[i]))
    for s in selInfo.keys():
        c = selInfo[s]
        if s is None or c is None:
            raise Exception("Invalid ServerID/ChannelID.")
        shouldsend = config.emget(s.id)
        try:
            if shouldsend("sendnewmsg") != "False":
                await SetupSelectionMsg((c, s))
            elif shouldsend("sendnewmsg") == "False":
                raise MessageAlreadyExists("Message in "+str(s)+" already exists!")
        except MessageAlreadyExists:
            pass


@client.event
async def on_message(message):
    if str(message.author) == str(client.user):
        em_info = config.emget(message.guild.id)
        roleemoji = em_info("roleemoji").split("|")
        for i in roleemoji:
            await message.add_reaction(i)
        config.emset(message.guild.id, "sendnewmsg", "False")


@client.event
async def on_raw_reaction_add(payload):
    channel = client.get_channel(int(payload.channel_id))
    message = await channel.fetch_message(int(payload.message_id))
    emoji = payload.emoji
    user = message.guild.get_member(int(payload.user_id))
    if user == client.user:
        pass
    elif str(message.channel.id) in config.get("SelectionChannelID").split("|"):
        em_info = config.emget(message.guild.id)
        roles = em_info("roles").split("|")
        roleemoji = em_info("roleemoji").split("|")
        if str(emoji) in roleemoji:
            roletoset = roles[roleemoji.index(str(emoji))]
            allroles = message.guild.roles
            for r in allroles:
                if roletoset == r.name:
                    roletoset = r
                    break
            try:
                await user.add_roles(roletoset, reason="Role Selection Emoji")
                print("Gave " + str(user.name) + " the role: " + str(roletoset.name))
            except discord.errors.Forbidden:
                print("Cannot do this to " + str(user.name))


@client.event
async def on_raw_reaction_remove(payload):
    channel = client.get_channel(int(payload.channel_id))
    message = await channel.fetch_message(int(payload.message_id))
    emoji = payload.emoji
    user = message.guild.get_member(int(payload.user_id))
    if user == client.user:
        pass
    elif str(channel.id) in config.get("SelectionChannelID").split("|"):
        em_info = config.emget(message.guild.id)
        roles = em_info("roles").split("|")
        roleemoji = em_info("roleemoji").split("|")
        if str(emoji) in roleemoji:
            roletorem = roles[roleemoji.index(str(emoji))]
            allroles = message.guild.roles
            for r in allroles:
                if roletorem == r.name:
                    roletorem = r
                    break
            try:
                await user.remove_roles(roletorem, reason="Role Selection Emoji")
                print("Removed the role: " + str(roletorem.name) + " from " + str(user.name))
            except discord.errors.Forbidden:
                print("Cannot do this to " + str(user.name))


token = config.get("BotSecret")
client.run(token)
