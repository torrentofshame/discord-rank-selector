import configparser, codecs


class Config:

    def __init__(self):
        config = configparser.ConfigParser()
        config.read_file(codecs.open("config.ini", "r", "utf8"))
        self.rawdata = config

    def get(self, ConfigProperty: str):
        return self.rawdata["Config"][ConfigProperty]

    def emget(self, guildid: int):
        return lambda confprop: self.rawdata[str(guildid)][confprop]

    def emset(self, guildid: int, key: str, value):
        self.rawdata.set(str(guildid), key, value)
        with codecs.open("config.ini", "w", "utf8") as newconfig:
            self.rawdata.write(newconfig)
        print("Config Updated Successfully!")


config = Config()
